import scrapy
import json
import pandas as pd

csv = pd.read_csv("../../cursos.csv")

def encontra_domains(texto):
    return [t.split('/')[2] for t in texto]
    
class capituradorLinks(scrapy.Spider):

    name = "capiturador"
    antes = ''

    compara = {
        "curso": [],
        "chave": ["curricular", "fluxo de habilitação", "programa completo", "intinerário"]
    }

    for c in csv["Nome do Curso"]:
        if c.lower() in antes: continue
        compara["curso"].append(c.lower())
        antes = c.lower()

    allowed_domains = encontra_domains(csv["Dominio"].dropna()) #cpcs.ufms.br, 'www.uel.br', 'icet.ufam.edu.br'
    start_urls = [i for i in csv["Dominio"].dropna()] #https://cpcs.ufms.br/, 'http://www.uel.br/', 'https://icet.ufam.edu.br/'

    # visited_urls = []

    def parse(self, response):
        links = response.css("a::attr(href)").getall()
        for link in links:
            lista = []

            if link[0] == "#" or "javascript" in link: continue
            
            lista.append(self.encontra(response.css("title::text").get(), self.compara))
            lista.append(self.encontra(response.xpath('//body/*[not(self::header) and not(self::footer) and not(self::aside) and not(self::nav)]').extract(), self.compara))

            if "noticia" in response.url or "revista" in response.url or "artigo" in response.url or "article" in response.url or "arquivo" in response.url or "evento" in response.url or "video" in response.url or "tv" in response.url or "site" in response.url or "upload" in response.url or "file" in response.url or "image" in response.url: continue

            if lista[0] is None:
                if lista[1] is None:
                    yield response.follow(link, callback=self.parse)
                    continue 

            a_filtrado = response.xpath('//body/*[not(self::header) and not(self::footer) and not(self::aside) and not(self::nav)]//a/@href').extract()

            pdfs = [a for a in a_filtrado if ".pdf" in a]
            for u in self.start_urls:
                if u not in response.url: continue
                nome = csv.loc[csv["Dominio"] == u]["Instituicao"].iloc[0]

            yield {
                'Instituição': nome,
                'curos': lista[1],
                'url': response.url,
                'pdfs': pdfs
            }
                
            yield response.follow(link, callback=self.parse)

            break

    # def closed(self, reason):
    #     with open('teste.json', 'w') as obj:
    #         obj.write(json.dumps(self.visited_urls))


    # Errado
    # def encontra(self, texto, compara):
    #     if isinstance(texto, list):
    #         for c in compara["chave"]:
    #             for t in texto:
    #                 disciplina = True if c in t.lower() else False
    #                 curso = True if compara["curso"][0] in t.lower() else False
    #                 return True if disciplina and curso else False
    #     else:
    #         for c in compara["chave"]:
    #             text1 = True if c in texto.lower() else False
    #             text2 = True if compara["curso"][0] in texto.lower() else False
    #             return True if text1 and text2 else False

    def encontra(self, texto, compara):
        if isinstance(texto, list):
            for c in compara["chave"]:
                for cr in compara["curso"]:
                    for t in texto:
                        chave = True if c in t.lower() else False
                        curso = True if cr in t.lower() else False
                        if chave and curso:
                            return cr
            
            return None
        else:
            for c in compara["chave"]:
                for cr in compara["curso"]:
                    text1 = True if c in texto.lower() else False
                    text2 = True if cr in texto.lower() else False
                    if text1 and text2:
                        return cr
            
            return None

    # def closed(self, reason):
    #     print(f'\n\n\n\n\n{self.compara["curso"]} \nallowed_domains: {self.allowed_domains} \nstart_urls: {self.start_urls}')
